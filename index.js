var lines = require('fs').readFileSync('log30.txt', 'utf-8')
.split('\r\n')
.filter(Boolean);

var chegadasLines = []
var saidasLines = []

lines.forEach(e => {
    if(e.indexOf('chegada') !== -1)
        chegadasLines.push(e)
    else
        saidasLines.push(e)
})

function getMin(str) {
    var minuto = str
    for(var i = 0; i < 4; i++) {
        minuto = minuto.substring(minuto.indexOf(':') + 1, minuto.length)
    }
    return minuto
}

function getSecond(milli) {
    return milli / 1000;
}

function getMedia(arr) {
    var qtd = [0]
    var counter = 0;
    var soma = 0;

    for(var i = 0; i < arr.length - 1; i++) {
        if(arr[i] != arr[i+1]) {
            soma += qtd[counter]
            qtd.push(1)
            counter++
        } else {
            qtd[counter]++
        }
    }

    return soma / qtd.length
}

function main () {
    var chegadas = []
    var saidas = []

    chegadasLines.forEach(e => {
        chegadas.push(parseFloat(getMin(e)))
    })

    saidasLines.forEach(e => {
        saidas.push(parseFloat(getMin(e)))
    })

    var lambda = parseInt(getMedia(chegadas))
    var taxaServico = parseInt(getMedia(saidas))
    
    var media = 0
    for(var i = 0; i < chegadas.length; i++) {
        media += saidas[i] - chegadas[i]
    }
    
    var tempoServico = ((media / chegadas.length) * 1000)
    var probabilidade50 = (Math.pow((lambda / taxaServico), 50) * ((taxaServico - lambda) / lambda))
    var probabilidade100 = (Math.pow((lambda / taxaServico), 100) * ((taxaServico - lambda) / lambda))
    var probabilidade150 = (Math.pow((lambda / taxaServico), 150) * ((taxaServico - lambda) / lambda))

    var probabilidadeOcioso = (taxaServico - lambda) / taxaServico;
    var numMedioCliente = parseInt(lambda / (taxaServico - lambda))
    var numMedioClienteFila = parseInt(Math.pow(lambda, 2) / (taxaServico * (taxaServico - lambda)))
    var tempoMedioEsperaFila = lambda / (taxaServico * (taxaServico - lambda))
    var tempoMedioGastoPorCliente = 1 / (taxaServico - lambda)
    
    console.log("*************************************************\n")
    console.log('Taxa chegada: ' + lambda)
    console.log("Taxa de serviço: " + taxaServico)
    console.log("\n*************************************************\n")
    console.log("Probabilidade de haver 50 clientes: " + probabilidade50)
    console.log("Probabilidade de haver 100 clientes: " + probabilidade100)
    console.log("Probabilidade de haver 150 clientes: " + probabilidade150)
    console.log("\n*************************************************\n")
    console.log("Probabilidade de que o sistema esteja ocioso: " + probabilidadeOcioso)
    console.log("Numero médio de clientes no sistema: " + numMedioCliente)
    console.log("Numero médio de clientes na fila: " + numMedioClienteFila)
    console.log("Tempo médio de espera na fila por cliente: " + tempoMedioEsperaFila)
    console.log("Tempo médio gasto no sistema por cliente: " + tempoMedioGastoPorCliente)
    console.log("\n*************************************************\n")
}

main()