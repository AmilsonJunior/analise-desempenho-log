var sqlite3 = require('sqlite3').verbose();
var bodyParser = require('body-parser')
var express = require('express');
var app = express();
var db = new sqlite3.Database('clientes.sqlite3');
var fs = require('fs');
var moment = require('moment')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

db.serialize(() => {
    db.run("CREATE TABLE IF NOT EXISTS CLIENTES (nome TEXT, telefone TEXT)");
});

app.get('/clientes/:nome', (req, res) => {
    var userid = parseInt((Math.random() * 1000));
    var chegadaDate = moment().format('MMMM Do YYYY, h:mm:ss:SSSS');
    fs.appendFileSync('logGetClient.txt', 'chegada id:' + userid + ' data: ' + chegadaDate + '\n')
    
    var nome = req.params.nome
    var result = []

    db.all("SELECT * FROM CLIENTES WHERE nome = $nome", {$nome: nome}, (err, rows) => {
        if (err)
            console.log(err);
        res.send(rows);
        var saidaDate = moment().format('MMMM Do YYYY, h:mm:ss:SSSS');
        fs.appendFileSync('logGetClient.txt', 'saida id:' + userid + ' data: ' + saidaDate + '\n')
    })
})

app.post('/clientes', (req, res) => {
    var userid = parseInt((Math.random() * 1000));
    var chegadaDate = moment().format('MMMM Do YYYY, h:mm:ss:SSSS');
    fs.appendFileSync('logPostClient.txt', 'chegada id:' + userid + ' data: ' + chegadaDate + '\n')

    var nome = req.body.nome;
    var telefone = req.body.telefone;

    db.run("INSERT INTO CLIENTES VALUES($nome, $telefone)", {
        $nome: nome,
        $telefone: telefone
    })

    res.send({
        nome,
        telefone
    })

    var saidaDate = moment().format('MMMM Do YYYY, h:mm:ss:SSSS');
    fs.appendFileSync('logPostClient.txt', 'saida id:' + userid + ' data: ' + saidaDate + '\n')
})

app.listen(3000, () => {
    console.log('Application running!')
})

